libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.6"

mainClass in Compile := Some("blast.compiler.Blastc")

enablePlugins(PackPlugin)
packMain := Map("blastc" -> "blast.compiler.Blastc")