package blast.meta

class Model {

  private var types:List[BUserDefinedType] = Nil
  private var commands:List[BCommand] = Nil

  def getType(name:String) : Option[BType] = {
    name match {
      case "string" =>
        Option.apply(BString)
      case "number" =>
        Option.apply(BNumber)
      case "boolean" =>
        Option.apply(BBoolean)
      case _ =>
        val filtered = getTypes.filter(_.name == name)
        if (filtered.isEmpty) {
          Option.empty
        } else {
          Option.apply(filtered.head)
        }
    }
  }

  def getTypes : List[BUserDefinedType] = types
  def getCommands : List[BCommand] = commands

  def getStruct(name:String):Option[BStruct] = {
    Option(
      types
        .filter(_.isInstanceOf[BStruct])
        .map(_.asInstanceOf[BStruct])
        .head
    )
 }

  def addStruct(name:String):BStruct = {
    val s = BStruct(name)
    addType(s)
  }

  def addEnum(name:String, values:String*): BEnum = {
    val e = BEnum(name, values.toList)
    addType(e)
  }

  def addCommand(name:String, inputType:BStruct, outputType:BStruct) : BCommand = {
    if (!commands.exists(_.name == name)) {
      val cmd = BCommand(name, inputType, outputType)
      commands = commands :+ cmd
      cmd
    } else {
      throw new IllegalArgumentException(s"a command with name $name is already defined")
    }

  }

  def addType[T <: BUserDefinedType](t:T): T = {
    // cannot add a struct if a type already
    // exists for this name
    if (getType(t.name).isEmpty) {
      types = types :+ t
      t
    } else {
      throw new IllegalArgumentException(s"a type with name ${t.name} is already defined")
    }
  }

  def toDsl:String = types.map(_.toDsl).mkString("\n")

  def canEqual(a: Any):Boolean = a.isInstanceOf[Model]

  override def equals(that: Any): Boolean =
    that match {
      case that: Model => that.canEqual(this) && this.hashCode == that.hashCode
      case _ => false
    }

  override def hashCode: Int = {
    toDsl.hashCode
  }

}

sealed trait BType {
  val name:String
  val toDsl:String
}

sealed trait BPrimitiveType extends BType {
  override val toDsl:String = name
}

case object BString extends BPrimitiveType {
  override val name:String = "string"
}

case object BBoolean extends BPrimitiveType {
  override val name:String = "boolean"
}

case object BNumber extends BPrimitiveType {
  override val name:String = "number"
}

sealed trait BUserDefinedType extends BType

sealed case class BEnum(name:String, values: List[String]) extends BUserDefinedType {

  override val toDsl:String = {
    s"""enum $name {${values.map("\n  " + _).mkString("")}
       |}
     """.stripMargin
  }

}

sealed case class BStruct(name: String) extends BUserDefinedType {

  private var attrs: List[BAttr] = Nil

  def addAttribute(bAttr:BAttr) : BStruct = {
    if (getAttribute(bAttr.name).nonEmpty) {
      throw new IllegalArgumentException(s"an attribute with name ${bAttr.name} is already defined for struct $name")
    }
    attrs = attrs :+ bAttr
    this
  }

  def getAttributes:List[BAttr] = attrs

  def getAttribute(name:String):Option[BAttr] = {
    val filtered = attrs.filter(_.name == name)
    if (filtered.isEmpty) {
      Option.empty
    } else {
      Option.apply(filtered.head)
    }
  }

  override val toDsl:String = {
    s"""struct $name {${attrs.map("\n  " + _.toDsl).mkString("")}
       |}
     """.stripMargin
  }

}

case class BAttr(name:String, attrType: BType, isArray: Boolean = false) {

  val toDsl:String = {
    val arraySuffix = if (isArray) "*" else ""
    name + ":" + attrType.name + arraySuffix
  }

}

final case class BCommand(name:String, input:BStruct, output:BStruct) {
  val toDsl = s"command $name : ${input.name} -> ${output.name}"
}