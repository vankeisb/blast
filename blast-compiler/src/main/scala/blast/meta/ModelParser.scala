package blast.meta

import java.io.StringReader

import scala.util.Try
import scala.util.parsing.combinator._

object ModelParser extends JavaTokenParsers {

  private def modelDef : Parser[ModelDef] = rep(struct | enum | command) ^^
  {
    ModelDef(_)
  }

  private def struct : Parser[StructDef] = "struct" ~ className ~ "{" ~ structBody ~ "}" ^^
  {
    case _ ~ structName ~ _ ~ structBody ~ _ =>
      StructDef(structName, structBody)
  }

  private def className : Parser[String] = repsep(ident, ".") ^^ { _.mkString(".") }

  private def structBody : Parser[List[AttrDef]] = rep(structAttr)

  private def structAttr: Parser[AttrDef] = ident ~ ":" ~ structAttrType ~ "*".? ^^
  {
    case attrName ~ _ ~ attrType ~ asterix =>
      AttrDef(attrName, attrType, asterix.isDefined)
  }

  private def structAttrType : Parser[String] = repsep(ident, ".") ^^ { _.mkString(".") }

  private def enum : Parser[EnumDef] = "enum" ~ className ~ "{" ~ rep(ident) ~"}" ^^
  {
    case _ ~ enumName ~ _ ~ values ~ _ =>
      EnumDef(enumName, values)
  }

  private def command : Parser[CmdDef] = "command" ~ className ~ ":" ~ className ~ "->" ~ className ^^
  {
    case _ ~ commandName ~ _ ~ inputType ~ _ ~ outputType =>
      CmdDef(commandName, inputType, outputType)
  }


  private def fail(msg:String) : Try[Model] = scala.util.Failure(new IllegalStateException(msg))

  private def defToModel(modelDef:ModelDef) : Try[Model] = {
    val model = new Model()
    var errs = List[String]()

    // pre-declare all types
    modelDef.types.foreach { s =>
      if (model.getType(s.name).isEmpty) {
        s match {
          case StructDef(_,_) =>
            model.addStruct(s.name)
          case EnumDef(name,values) =>
            model.addType(BEnum(name, values))
        }
      } else {
        errs = s"duplicated type definition ${s.name}" :: errs
      }
    }
    if (errs.nonEmpty) {
      fail(errs.mkString("\n"))
    } else {

      errs = List[String]()
      // now handle attributes
      modelDef.structs.foreach { s =>
          val struct = model.getType(s.name).get.asInstanceOf[BStruct]
          s.attrs.foreach(attrDef => {
            val attrType = model.getType(attrDef.attrType)
            if (attrType.isEmpty) {
              errs = s"undefined attribute type $attrType" :: errs
            } else {
              struct.addAttribute(
                BAttr(attrDef.name, attrType.get, attrDef.isArray)
              )
            }
          })
        }
      if (errs.nonEmpty) {
        fail(errs.mkString("\n"))
      } else {

        // good, now handle commands
        modelDef.commands.foreach { cmd =>
          val sIn = model.getStruct(cmd.inputType)
          if (sIn.isEmpty) {
            errs = s"input struct not found for command $cmd" :: errs
          } else {
            val sOut = model.getStruct(cmd.outputType)
            if (sOut.isEmpty) {
              errs = s"output struct not found for command $cmd" :: errs
            } else {
              model.addCommand(cmd.name, sIn.get, sOut.get)
            }
          }
        }

        scala.util.Success(model)
      }
    }
  }

  // entry point
  def parseModel(text:java.io.Reader) : Try[Model] = {
    ModelParser.this.parseAll(modelDef, text) match {
      case Success(result, _) => defToModel(result)
      case Failure(msg, _) => fail(msg)
      case Error(msg, _) => fail(msg)
    }
  }

  def parseModel(text:String) : Try[Model] = {
    val reader = new StringReader(text)
    try {
      parseModel(reader)
    } finally {
      reader.close()
    }
  }

}

protected sealed case class ModelDef(elements:List[ModelElemDef]) {
  val types:List[TypeDef] = elements.filter(_.isInstanceOf[TypeDef]).map(_.asInstanceOf[TypeDef])
  val structs:List[StructDef] = elements.filter(_.isInstanceOf[StructDef]).map(_.asInstanceOf[StructDef])
  val enums:List[EnumDef] = elements.filter(_.isInstanceOf[EnumDef]).map(_.asInstanceOf[EnumDef])
  val commands:List[CmdDef] = elements.filter(_.isInstanceOf[CmdDef]).map(_.asInstanceOf[CmdDef])
}
protected sealed case class AttrDef(name:String,attrType:String,isArray:Boolean)
protected sealed trait ModelElemDef {
  def name:String
}
protected sealed trait TypeDef extends ModelElemDef
protected sealed case class StructDef(name:String, attrs:List[AttrDef]) extends TypeDef
protected sealed case class EnumDef(name:String, values:List[String]) extends TypeDef
protected final case class CmdDef(name:String, inputType:String, outputType:String) extends ModelElemDef