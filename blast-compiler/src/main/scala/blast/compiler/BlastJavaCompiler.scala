package blast.compiler

import java.io.{File, FileReader, FileWriter, Writer}

import blast.meta._

import scala.util.Try

object BlastJavaCompiler extends BlastCompiler {

  override def compile(opts:Opts) : Try[CompilerResult] = {

    if (!opts.outputDirectory.exists()) {
      opts.outputDirectory.mkdirs()
    }

    if (!opts.modelFile.exists()) {
      throw new IllegalStateException(s"model file ${opts.modelFile.getAbsolutePath} does not exist")
    }

    ModelParser.parseModel(new FileReader(opts.modelFile))
      .map(model => {
        val typeFiles =
          model.getTypes.map {
            case e @ BEnum(enumName, values) =>
              withJavaFile(opts, e) { out =>
                generateEnum(e, out)
              }
            case s @ BStruct(structName) =>
              withJavaFile(opts, s) { out =>
                generateStruct(s, out)
              }
          }
        val commandFiles =
          model.getCommands.map { cmd =>
            withJavaFile(opts, cmd) { out =>
              generateCmd(cmd, out)
            }
          }
        new CompilerResult(
          typeFiles
        )
      })
  }

  private def withFile(opts:Opts, relPath:String)(block: Writer => Unit) : File = {
    val outFilePath = opts.outputDirectory.getAbsolutePath + File.separator + relPath
    val outFile = new File(outFilePath)
    if (!outFile.exists()) {
      outFile.getParentFile.mkdirs()
    }
    val out = new FileWriter(outFile)
    try {
      block(out)
    } finally {
      out.close()
    }
    outFile
  }

  private def withJavaFile(opts:Opts, cmd: BCommand)(block: Writer => Unit) : File = {
    val pkgAndClassName = exractPackageAndClassName(cmd.name)
    val relPath = toPath(pkgAndClassName._1) + File.separator + pkgAndClassName._2 + "Base.java"
    withFile(opts, relPath)(block)
  }

  private def withJavaFile(opts:Opts, bType: BType)(block: Writer => Unit) : File = {
    val pkgAndClassName = exractPackageAndClassName(bType.name)
    val relPath = toPath(pkgAndClassName._1) + File.separator + pkgAndClassName._2 + ".java"
    withFile(opts, relPath)(block)
  }

  def generateEnum(enum: BEnum, out: Writer) : Unit = {
    val pac = exractPackageAndClassName(enum.name)
    out.write(
      s"""package ${pac._1};
         |public enum ${pac._2} {
         |  ${enum.values.mkString(",")};
         |}
         |""".stripMargin
    )
  }

  def generateCmd(cmd: BCommand, out: Writer) : Unit = {
    val pac = exractPackageAndClassName(cmd.name)
    val inType = cmd.input.name
    val outType = cmd.output.name
    out.write(
      s"""package ${pac._1};
         |
         |import blast.b4j.Command;
         |import java.io.IOException;
         |import java.io.Reader;
         |import java.io.Writer;
         |import javax.json.Json;
         |import javax.json.stream.JsonGenerator;
         |import javax.json.stream.JsonParser;
         |
         |public abstract class ${pac._2}Base implements Command<$inType,$outType> {
         |  @Override
         |  public Class<$inType> getInputType() {
         |    return $inType.class;
         |  }
         |  @Override
         |  public Class<$outType> getOutputType() {
         |    return $outType.class;
         |  }
         |  @Override
         |  public $inType deserializeInput(Reader reader) throws IOException {
         |    try (JsonParser p = Json.createParser(reader)) {
         |      return new $inType.Deserializer().deserialize(p);
         |    }
         |  }
         |  @Override
         |  public void serializeOutput($outType output, Writer out) throws IOException {
         |    try (JsonGenerator g = Json.createGenerator(out)) {
         |      new $outType.Serializer().serialize(output, g);
         |    }
         |  }
         |}
         |""".stripMargin
    )
  }

  def generateStruct(s: BStruct, out:Writer) : Unit = {
    val pac = exractPackageAndClassName(s.name)
    // package/class declaration
    val structClassName = pac._2

    def ln(line:String):Unit = {
      out.write(line)
      out.write("\n")
    }

    out.write(
      s"""package ${pac._1};
         |
         |import java.util.*;
         |import javax.json.stream.JsonGenerator;
         |import javax.json.stream.JsonParser;
         |import javax.json.Json;
         |import javax.json.JsonValue;
         |import static javax.json.stream.JsonParser.Event.*;
         |import blast.b4j.*;
         |
         |public final class $structClassName implements Struct {
         |""".stripMargin
    )
    // fields
    s.getAttributes.foreach(a => {
      ln(s"  private final ${getJavaType(a)} ${a.name};")
    })
    // constructor
    out.write(s"  public $structClassName(")
    out.write(
      s.getAttributes
        .map(a => s"${getJavaType(a)} ${a.name}")
          .mkString(", ")
    )
    ln(") {")
    s.getAttributes.foreach(a => {
      if (a.isArray) {
        ln(s"    this.${a.name} = ${a.name} == null ? null : new ArrayList<>(${a.name});")
      } else {
        ln(s"    this.${a.name} = ${a.name};")
      }
    })
    ln("  }")

    // accessors
    s.getAttributes.foreach(a => {
      val retClause =
        if (a.isArray) {
          s"Collections.unmodifiableList(this.${a.name})"
        } else {
          s"this.${a.name}"
        }
      out.write(
        s"""  public ${getJavaType(a)} get${uppercaseFirstLetter(a.name)}() {
           |    return $retClause;
           |  }
           |""".stripMargin
      )
    })

    ln("")

    // builder static class
    val builderClassName = "Builder"
    ln(s"  public static class $builderClassName {")
    s.getAttributes.foreach(a => {
      val attrJavaType = getJavaType(a)
      ln(s"    private $attrJavaType ${a.name};")
      ln(s"    public $builderClassName set${uppercaseFirstLetter(a.name)}($attrJavaType ${a.name}) {")
      ln(s"      this.${a.name} = ${a.name};")
      ln("      return this;")
      ln("    }")
    })
    ln(s"    public $structClassName build() {")
    val ctorArgs = s.getAttributes
        .map(_.name)
        .mkString(", ")
    ln(s"      return new $structClassName($ctorArgs);")
    ln(s"    }")
    ln("  }\n")

    // serializer static class
    val serializerClassName = "Serializer"
    def genWriteValue(a:BAttr) : Unit = {
      ln(s"""          g.write("${a.name}", Json.createValue(that.${a.name}));""")
    }
    def genWriteValueArray(a:BAttr) : Unit = {
      ln(s"""            g.write(Json.createValue(that.${a.name}.get(i)));""")
    }
    ln(s"  public static class $serializerClassName implements blast.b4j.Serializer<$structClassName> {")
    ln("    @Override")
    ln(s"    public void serialize($structClassName that,JsonGenerator g) {")
    ln("      try {")
    ln("        g.writeStartObject();\n")
    s.getAttributes.foreach(a => {
      ln(s"        if (that.${a.name} != null) {")
      if (a.isArray) {
        ln(s"""          g.writeStartArray("${a.name}");""")
        ln(s"""          for (int i = 0; i < that.${a.name}.size(); i++) {""")
        a.attrType match {
          case BString =>
            genWriteValueArray(a)
          case BBoolean =>
            ln(s"""            g.write(that.${a.name}.get(i) ? JsonValue.TRUE : JsonValue.FALSE);""")
          case BNumber =>
            genWriteValueArray(a)
          case BEnum(enumName, values) =>
            ln(s"          g.write(that.${a.name}.get(i));")
          case BStruct(structName) =>
            val attrJavaType = getJavaType(a)
            ln(s"          new $attrJavaType.Serializer().serialize(that.${a.name}.get(i), g);")
        }
        ln(s"""          }\n""")
        ln(s"          g.writeEnd();\n")
      } else {
        a.attrType match {
          case BString =>
            genWriteValue(a)
          case BBoolean =>
            ln(s"""          g.write("${a.name}", that.${a.name} ? JsonValue.TRUE : JsonValue.FALSE);""")
          case BNumber =>
            genWriteValue(a)
          case BEnum(enumName, values) =>
            ln(s"          g.write(that.${a.name});")
          case BStruct(structName) =>
            val attrJavaType = getJavaType(a)
            out.write(
              s"""          g.writeKey("${a.name}");
                 |          new $attrJavaType.Serializer().serialize(that.${a.name}, g);
                 |""".stripMargin
            )
        }
      }
      ln("        }\n")
    })
    ln("        g.writeEnd();")
    ln("      } catch(Exception e) {")
    ln("""        throw new SerializationException("Error serializing " + that, e);""")
    ln("      }")
    ln("    }")
    ln("  }\n")

    // deserializer static class
    val deserializerClassName = "Deserializer"
    out.write(
      s"""  public static class $deserializerClassName implements blast.b4j.Deserializer<$structClassName> {
        |    @Override
        |    public $structClassName deserialize(JsonParser parser) throws SerializationException {
        |      $builderClassName builder = new $builderClassName();
        |      String key = null;
        |      while(parser.hasNext()) {
        |        JsonParser.Event e = parser.next();
        |        if (e == END_OBJECT) {
        |          break;
        |        } else if (e == KEY_NAME) {
        |          key = parser.getString();
        |        } else {
        |""".stripMargin
    )
    s.getAttributes.foreach(attr => {
      ln(s"""          if ("${attr.name}".equals(key)) {""")
      val javaType = getJavaType(attr)
      if (attr.isArray) {
        out.write(
          s"""            $javaType values = new ArrayList<>();
             |            boolean arrayEnd = false;
             |            while (parser.hasNext() && !arrayEnd) {
             |              JsonParser.Event ae = parser.next();
             |              switch(ae) {
             |                case END_ARRAY:
             |                  arrayEnd = true;
             |                  break;
             |                case VALUE_NULL:
             |                  values.add(null);
             |                  break;
             |""".stripMargin
        )
        attr.attrType match {
          case BStruct(name) =>
            out.write(
              s"""                case START_OBJECT :
                 |                  $javaType item = new ${javaType}Deserializer().deserialize(parser);
                 |                  break;
               """.stripMargin
            )
          case BEnum(name, values) =>
            out.write(
              s"""                case VALUE_STRING :
                 |                  values.add($javaType.valueOf(parser.getString()));
                 |                  break;
                 |""".stripMargin
            )
          case BBoolean =>
            out.write(
              s"""                case VALUE_TRUE :
                 |                  values.add(true);
                 |                  break;
                 |                case VALUE_FALSE :
                 |                  values.add(false);
                 |                    break;
                 |""".stripMargin
            )
          case BString =>
            out.write(
              s"""                case VALUE_STRING :
                 |                  values.add(parser.getString());
                 |                  break;
                 |""".stripMargin
            )
          case BNumber =>
            out.write(
              s"""                case VALUE_NUMBER :
                 |                  values.add(parser.getBigDecimal().doubleValue());
                 |                  break;
                 |""".stripMargin
            )
        }
        out.write(
          s"""                default:
             |                  throw new SerializationException("unexpected event " + ae);
             |              }
             |            }
             |            builder.set${uppercaseFirstLetter(attr.name)}(values);
             |""".stripMargin
        )
      } else {
        val builderSet = s"builder.set${uppercaseFirstLetter(attr.name)}"
        attr.attrType match {
          case BStruct(name) =>
            ln(s"            $builderSet(new $javaType.Deserializer().deserialize(parser));")
          case BEnum(name, values) =>
            ln(s"            $builderSet($javaType.valueOf(parser.getString());")
          case primitive @ _ =>
            ln("            switch(e) {")
            primitive match {
              case BBoolean =>
                out.write(
                  s"""              case VALUE_TRUE:
                     |                $builderSet(true);
                     |                break;
                     |              case VALUE_FALSE:
                     |                $builderSet(false);
                     |                break;
                     |""".stripMargin
                )
              case BString =>
                out.write(
                  s"""              case VALUE_STRING:
                     |                $builderSet(parser.getString());
                     |                break;
                     |""".stripMargin
                )
              case BNumber =>
                out.write(
                  s"""            case VALUE_NUMBER:
                     |              $builderSet(parser.getBigDecimal().doubleValue());
                     |              break;
                     |""".stripMargin
                )
            }
            out.write(
              """              default:
                |                throw new SerializationException("unexpected event " + e);
                |""".stripMargin
            )
            ln("            }")
        }
      }
      ln("          }")
    })
    ln(s"        }")
    ln(s"      }")
    ln(s"      return builder.build();")
    ln(s"    }")
    ln(s"  }") // end of struct builder



    // end of struct class
    ln("}")
  }

  private def getJavaType(attr: BAttr) : String = {
    val javaType = getJavaType(attr.attrType)
    if (attr.isArray) s"List<$javaType>" else javaType
  }

  private def getJavaType(bType: BType) : String = {
    bType match {
      case BString => "String"
      case BBoolean => "Boolean"
      case BNumber => "Double"
      case BEnum(enumName, values) => enumName
      case BStruct(structName) => structName
    }
  }

}
