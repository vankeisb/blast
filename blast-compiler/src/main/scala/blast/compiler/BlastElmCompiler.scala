package blast.compiler

import java.io.{File, FileReader, FileWriter, Writer}

import blast.meta._

import scala.util.Try

object BlastElmCompiler extends BlastCompiler {

  override def compile(opts: Opts) : Try[CompilerResult] = {
    if (!opts.outputDirectory.exists()) {
      opts.outputDirectory.mkdirs()
    }

    if (!opts.modelFile.exists()) {
      throw new IllegalStateException(s"model file ${opts.modelFile.getAbsolutePath} does not exist")
    }

    val fileName = opts.modelFile.getName
    val moduleName = uppercaseFirstLetter(fileName.substring(0, fileName.length - ".blast".length))

    val outFilePath = opts.outputDirectory.getAbsolutePath + File.separator + s"$moduleName.elm"
    val outFile = new File(outFilePath)
    if (!outFile.exists()) {
      outFile.getParentFile.mkdirs()
    }
    val out = new FileWriter(outFile)
    try {
      out.write(
        s"""module Blast.$moduleName exposing (..)
           |
           |import Json.Decode exposing (..)
           |
           |""".stripMargin)

      ModelParser.parseModel(new FileReader(opts.modelFile))
        .map(model => {
            model.getTypes.foreach {
              case e@BEnum(enumName, values) =>
                generateEnum(e, out)
                out.write("\n\n")
                generateDecoder(e, out)
                out.write("\n\n")
              case s@BStruct(structName) =>
                generateStruct(s, out)
                out.write("\n\n")
                generateDecoder(s, out)
                out.write("\n\n")
            }
            new CompilerResult(List(outFile))
        })
    } finally {
      out.close()
    }
  }

  def generateEnum(e:BEnum, out:Writer) : Unit = {
    val tagName = toElmIdentifier(e.name)
    out.write(s"type $tagName\n  = ")
    val tags = e.values.map(v => s"$v").mkString("\n  | ")
    out.write(tags)
    out.write("\n")
  }

  def generateDecoder(e:BEnum, out:Writer) : Unit = {
    val tagName = toElmIdentifier(e.name)
    val decoderFunctionName = s"${lowercaseFirstLetter(tagName)}Decoder"
    out.write(
      s"""$decoderFunctionName : String -> Decoder $tagName
         |$decoderFunctionName s =
         |  case s of
         |""".stripMargin
    )
    e.values.foreach(v => {
      out.write(s"""    "$v" ->\n""")
      out.write(s"""      succeed $v\n""")
    })
    out.write(
      """    _ ->
        |      fail <| "unknown enum value " ++ s
        |""".stripMargin)
  }

  def generateDecoder(s:BStruct, out:Writer) : Unit = {
    val structName = toElmIdentifier(s.name)
    val decoderFunctionName = s"${lowercaseFirstLetter(structName)}Decoder"
    val nbFields =
      if (s.getAttributes.size > 1) s.getAttributes.size.toString else ""
    out.write(
      s"""$decoderFunctionName : Decoder $structName
         |$decoderFunctionName =
         |  map$nbFields $structName
         |""".stripMargin
    )
    s.getAttributes.foreach { a =>
      val listPrefix =
        if (a.isArray) {
          "list "
        } else {
          ""
        }
      val decoder = a.attrType match {
        case BBoolean =>
          "bool"
        case BNumber =>
          "float"
        case BString =>
          "string"
        case BStruct(name) =>
          s"${lowercaseFirstLetter(toElmIdentifier(name))}Decoder"
        case BEnum(name, _) =>
          s"${lowercaseFirstLetter(toElmIdentifier(name))}Decoder"
      }
      out.write(s"""    (field "${a.name}" <| $listPrefix$decoder)\n""")
    }
  }


  def generateStruct(s:BStruct, out:Writer) : Unit = {
    val structName = toElmIdentifier(s.name)
    out.write(s"type alias $structName =\n  { ")
    val attrs = s.getAttributes.map(a => s"${a.name} : ${getElmType(a)}").mkString("\n  , ")
    out.write(attrs)
    out.write("\n  }\n")
  }

  def getElmType(attr:BAttr) : String = {
    val t = attr.attrType match {
      case BString =>
        "String"
      case BBoolean =>
        "Bool"
      case BNumber =>
        "Float"
      case BStruct(name) =>
        toElmIdentifier(name)
      case BEnum(name, _) =>
        toElmIdentifier(name)
    }
    if (attr.isArray) {
      s"List $t"
    } else {
      t
    }
  }

  private def toElmIdentifier(fqn:String) : String = exractPackageAndClassName(fqn)._2

}
