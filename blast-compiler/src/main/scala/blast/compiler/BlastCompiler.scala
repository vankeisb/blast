package blast.compiler

import java.io.{File, FileWriter, Writer}

import scala.util.{Failure, Success, Try}

class CompilerResult(val generatedFiles:List[File])

abstract class BlastCompiler {
  def compile(opts:Opts) : Try[CompilerResult]

  def uppercaseFirstLetter(str: String) : String = {
    val first = str.substring(0, 1)
    val rest = str.substring(1)
    first.toUpperCase + rest
  }

  def lowercaseFirstLetter(str: String) : String = {
    val first = str.substring(0, 1)
    val rest = str.substring(1)
    first.toLowerCase + rest
  }

  def toPath(dottedStr:String) : String = dottedStr.split("\\.").toList.mkString(File.separator)

  def exractPackageAndClassName(str: String): (String,String) = {
    val i = str.lastIndexOf('.')
    if (i == -1) {
      ("", str)
    } else {
      (str.substring(0, i), str.substring(i + 1))
    }
  }

}

object Blastc {

  val compilers = Map(
    "java" -> BlastJavaCompiler,
    "elm" -> BlastElmCompiler
  )

  def version:String = System.getProperty("prog.version", "DEV")

  def compile(opts: Opts, lang:String) : Try[CompilerResult] = {
    println(s"""
               |______________             _____
               |___  __ )__  /_____ _________  /_
               |__  __  |_  /_  __ `/_  ___/  __/
               |_  /_/ /_  / / /_/ /_(__  )/ /_
               |/_____/ /_/  \\__,_/ /____/ \\__/  v$version
               |""".stripMargin)

    val compiler = compilers(lang)
    if (compiler == null) {
      println(s"Unsupported lang : $lang")
      println(usage)
      Failure(new IllegalArgumentException())
    } else {
      compiler.compile(opts)
    }
  }

  def main(args:Array[String]) : Unit = {
    parseOpts(args) match {
      case Success((opts,lang)) =>
        println(
          s"""- output : ${opts.outputDirectory.getAbsolutePath}
             |- lang   : $lang
           """.stripMargin)

        // find a suitable compiler
        compile(opts, lang) match {
          case Success(compilerResult) =>
            println("Compilation ok, files generated :")
            println(
              compilerResult.generatedFiles
                  .map(f => s"- ${f.getAbsolutePath}")
                  .mkString("\n")
            )
          case Failure(err) =>
            println("ERROR !")
            err.printStackTrace()
            System.exit(1)
        }

      case Failure(err) =>
        println(s"Invalid option(s) : ${err.getMessage}")
        println(usage)
        System.exit(1)
    }
  }

  def parseOpts(args:Array[String]) : Try[(Opts,String)] = {
    var outputDir:File = null
    var lang:String = null
    var modelFile:File = null
    def nextOption(lst:List[String]) : Unit = {
      lst match {
        case "-l" :: value :: tail =>
          lang = value
          nextOption(tail)
        case "-o" :: value :: tail =>
          outputDir = new File(value)
          nextOption(tail)
        case str :: Nil =>
          modelFile = new File(str)
        case _ =>
          // no-op
      }
    }

    nextOption(args.toList)

    def fail(msg:String) : Try[Opts] = Failure(new IllegalArgumentException(msg))

    val opts =
      if (outputDir == null || lang == null) {
        fail("output dir and language must be provided")
      } else {
        if (modelFile == null) {
          fail("model file must be provided")
        } else {
          Success(Opts(outputDir, modelFile))
        }
      }

    opts.map(o => (o, lang))
  }

  def usage : String = s"""Usage : blastc -l ${compilers.keys.mkString("|")} -o output_dir blast_file"""
}

case class Opts(outputDirectory:File, modelFile: File)
