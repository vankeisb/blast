package blast.compiler

import java.io.StringWriter

import blast.BlastSpec
import blast.meta._

class BlastJavaCompilerTest extends BlastSpec {

  behavior of "A Java compiler"

  it should "compile a struct" in {
    val model: Model = new Model
    val myStruct =
      model
        .addStruct("com.myco.MyStruct")
        .addAttribute(BAttr("foo", BString))
        .addAttribute(BAttr("pStrings", BString, isArray = true))
        .addAttribute(BAttr("pNumber", BNumber))
        .addAttribute(BAttr("pBool", BBoolean))

    myStruct.addAttribute(BAttr("pSelf", myStruct))

    val out = new StringWriter()
    BlastJavaCompiler.generateStruct(myStruct, out)
    out.close()

    val str = out.toString

    assert(str ===
      """package com.myco;
        |
        |import java.util.*;
        |import javax.json.stream.JsonGenerator;
        |import javax.json.stream.JsonParser;
        |import javax.json.Json;
        |import javax.json.JsonValue;
        |import static javax.json.stream.JsonParser.Event.*;
        |import blast.b4j.*;
        |
        |public final class MyStruct implements Struct {
        |  private final String foo;
        |  private final List<String> pStrings;
        |  private final Double pNumber;
        |  private final Boolean pBool;
        |  private final com.myco.MyStruct pSelf;
        |  public MyStruct(String foo, List<String> pStrings, Double pNumber, Boolean pBool, com.myco.MyStruct pSelf) {
        |    this.foo = foo;
        |    this.pStrings = pStrings == null ? null : new ArrayList<>(pStrings);
        |    this.pNumber = pNumber;
        |    this.pBool = pBool;
        |    this.pSelf = pSelf;
        |  }
        |  public String getFoo() {
        |    return this.foo;
        |  }
        |  public List<String> getPStrings() {
        |    return Collections.unmodifiableList(this.pStrings);
        |  }
        |  public Double getPNumber() {
        |    return this.pNumber;
        |  }
        |  public Boolean getPBool() {
        |    return this.pBool;
        |  }
        |  public com.myco.MyStruct getPSelf() {
        |    return this.pSelf;
        |  }
        |
        |  public static class Builder {
        |    private String foo;
        |    public Builder setFoo(String foo) {
        |      this.foo = foo;
        |      return this;
        |    }
        |    private List<String> pStrings;
        |    public Builder setPStrings(List<String> pStrings) {
        |      this.pStrings = pStrings;
        |      return this;
        |    }
        |    private Double pNumber;
        |    public Builder setPNumber(Double pNumber) {
        |      this.pNumber = pNumber;
        |      return this;
        |    }
        |    private Boolean pBool;
        |    public Builder setPBool(Boolean pBool) {
        |      this.pBool = pBool;
        |      return this;
        |    }
        |    private com.myco.MyStruct pSelf;
        |    public Builder setPSelf(com.myco.MyStruct pSelf) {
        |      this.pSelf = pSelf;
        |      return this;
        |    }
        |    public MyStruct build() {
        |      return new MyStruct(foo, pStrings, pNumber, pBool, pSelf);
        |    }
        |  }
        |
        |  public static class Serializer implements blast.b4j.Serializer<MyStruct> {
        |    @Override
        |    public void serialize(MyStruct that,JsonGenerator g) {
        |      try {
        |        g.writeStartObject();
        |
        |        if (that.foo != null) {
        |          g.write("foo", Json.createValue(that.foo));
        |        }
        |
        |        if (that.pStrings != null) {
        |          g.writeStartArray("pStrings");
        |          for (int i = 0; i < that.pStrings.size(); i++) {
        |            g.write(Json.createValue(that.pStrings.get(i)));
        |          }
        |
        |          g.writeEnd();
        |
        |        }
        |
        |        if (that.pNumber != null) {
        |          g.write("pNumber", Json.createValue(that.pNumber));
        |        }
        |
        |        if (that.pBool != null) {
        |          g.write("pBool", that.pBool ? JsonValue.TRUE : JsonValue.FALSE);
        |        }
        |
        |        if (that.pSelf != null) {
        |          g.writeKey("pSelf");
        |          new com.myco.MyStruct.Serializer().serialize(that.pSelf, g);
        |        }
        |
        |        g.writeEnd();
        |      } catch(Exception e) {
        |        throw new SerializationException("Error serializing " + that, e);
        |      }
        |    }
        |  }
        |
        |  public static class Deserializer implements blast.b4j.Deserializer<MyStruct> {
        |    @Override
        |    public MyStruct deserialize(JsonParser parser) throws SerializationException {
        |      Builder builder = new Builder();
        |      String key = null;
        |      while(parser.hasNext()) {
        |        JsonParser.Event e = parser.next();
        |        if (e == END_OBJECT) {
        |          break;
        |        } else if (e == KEY_NAME) {
        |          key = parser.getString();
        |        } else {
        |          if ("foo".equals(key)) {
        |            switch(e) {
        |              case VALUE_STRING:
        |                builder.setFoo(parser.getString());
        |                break;
        |              default:
        |                throw new SerializationException("unexpected event " + e);
        |            }
        |          }
        |          if ("pStrings".equals(key)) {
        |            List<String> values = new ArrayList<>();
        |            boolean arrayEnd = false;
        |            while (parser.hasNext() && !arrayEnd) {
        |              JsonParser.Event ae = parser.next();
        |              switch(ae) {
        |                case END_ARRAY:
        |                  arrayEnd = true;
        |                  break;
        |                case VALUE_NULL:
        |                  values.add(null);
        |                  break;
        |                case VALUE_STRING :
        |                  values.add(parser.getString());
        |                  break;
        |                default:
        |                  throw new SerializationException("unexpected event " + ae);
        |              }
        |            }
        |            builder.setPStrings(values);
        |          }
        |          if ("pNumber".equals(key)) {
        |            switch(e) {
        |            case VALUE_NUMBER:
        |              builder.setPNumber(parser.getBigDecimal().doubleValue());
        |              break;
        |              default:
        |                throw new SerializationException("unexpected event " + e);
        |            }
        |          }
        |          if ("pBool".equals(key)) {
        |            switch(e) {
        |              case VALUE_TRUE:
        |                builder.setPBool(true);
        |                break;
        |              case VALUE_FALSE:
        |                builder.setPBool(false);
        |                break;
        |              default:
        |                throw new SerializationException("unexpected event " + e);
        |            }
        |          }
        |          if ("pSelf".equals(key)) {
        |            builder.setPSelf(new com.myco.MyStruct.Deserializer().deserialize(parser));
        |          }
        |        }
        |      }
        |      return builder.build();
        |    }
        |  }
        |}
        |""".stripMargin
    )
  }

  it should "compile a command base" in {
    val model: Model = new Model
    val myInput = model.addStruct("com.myco.MyInput").addAttribute(BAttr("foo", BString))
    val myOutput = model.addStruct("com.myco.MyOutput").addAttribute(BAttr("bar", BString))
    val myCmd = model.addCommand("com.myco.MyCommand", myInput, myOutput)
    val out = new StringWriter()
    BlastJavaCompiler.generateCmd(myCmd, out)
    out.close()

    val str = out.toString
    assert(str == """package com.myco;
                    |
                    |import blast.b4j.Command;
                    |import java.io.IOException;
                    |import java.io.Reader;
                    |import java.io.Writer;
                    |import javax.json.Json;
                    |import javax.json.stream.JsonGenerator;
                    |import javax.json.stream.JsonParser;
                    |
                    |public abstract class MyCommandBase implements Command<com.myco.MyInput,com.myco.MyOutput> {
                    |  @Override
                    |  public Class<com.myco.MyInput> getInputType() {
                    |    return com.myco.MyInput.class;
                    |  }
                    |  @Override
                    |  public Class<com.myco.MyOutput> getOutputType() {
                    |    return com.myco.MyOutput.class;
                    |  }
                    |  @Override
                    |  public com.myco.MyInput deserializeInput(Reader reader) throws IOException {
                    |    try (JsonParser p = Json.createParser(reader)) {
                    |      return new com.myco.MyInput.Deserializer().deserialize(p);
                    |    }
                    |  }
                    |  @Override
                    |  public void serializeOutput(com.myco.MyOutput output, Writer out) throws IOException {
                    |    try (JsonGenerator g = Json.createGenerator(out)) {
                    |      new com.myco.MyOutput.Serializer().serialize(output, g);
                    |    }
                    |  }
                    |}
                    |""".stripMargin)
  }

}
