package blast.compiler

import java.io.{StringWriter, Writer}

import blast.BlastSpec
import blast.meta._

class BlastElmCompilerTest extends BlastSpec {

  val model1: Model = new Model
  val myStruct : BStruct =
    model1
      .addStruct("com.myco.MyStruct")
      .addAttribute(BAttr("foo", BString))
      .addAttribute(BAttr("pStrings", BString, isArray = true))
      .addAttribute(BAttr("pNumber", BNumber))
      .addAttribute(BAttr("pBool", BBoolean))

  myStruct.addAttribute(BAttr("pSelf", myStruct))

  private def writeToString(block:Writer => Unit) : String = {
    val out = new StringWriter()
    try {
      block.apply(out)
    } finally {
      out.close()
    }
    out.toString
  }

  behavior of "A Elm compiler"

  it should "compile a struct" in {
    val str = writeToString { out =>
      BlastElmCompiler.generateStruct(myStruct, out)
    }
    println(str)
    assert(str === """type alias MyStruct =
                     |  { foo : String
                     |  , pStrings : List String
                     |  , pNumber : Float
                     |  , pBool : Bool
                     |  , pSelf : MyStruct
                     |  }
                     |""".stripMargin)
  }

  it should "compile a struct decoder" in {
    val str = writeToString { out =>
      BlastElmCompiler.generateDecoder(myStruct, out)
    }
    println(str)
    assert(str === """myStructDecoder : Decoder MyStruct
                     |myStructDecoder =
                     |  map5 MyStruct
                     |    (field "foo" <| string)
                     |    (field "pStrings" <| list string)
                     |    (field "pNumber" <| float)
                     |    (field "pBool" <| bool)
                     |    (field "pSelf" <| myStructDecoder)
                     |""".stripMargin)
  }

  it should "compile an enum" in {
    val model: Model = new Model
    val myEnum =
      model
        .addEnum("com.myco.MyEnum", "Value1", "Value2")

    val out = new StringWriter()
    BlastElmCompiler.generateEnum(myEnum, out)
    out.close()

    println(out.toString)

    assert(out.toString === """type MyEnum
                     |  = Value1
                     |  | Value2
                     |""".stripMargin)
  }

  it should "compile an enum decoder" in {
    val model: Model = new Model
    val myEnum =
      model
        .addEnum("com.myco.MyEnum", "Value1", "Value2")

    val out = new StringWriter()
    BlastElmCompiler.generateDecoder(myEnum, out)
    out.close()

    println(out.toString)

    assert(out.toString === """myEnumDecoder : String -> Decoder MyEnum
                              |myEnumDecoder s =
                              |  case s of
                              |    "Value1" ->
                              |      succeed Value1
                              |    "Value2" ->
                              |      succeed Value2
                              |    _ ->
                              |      fail <| "unknown enum value " ++ s
                              |""".stripMargin)
  }

}
