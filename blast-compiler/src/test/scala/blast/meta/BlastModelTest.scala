package blast.meta

import java.io.InputStreamReader

import blast.BlastSpec

import scala.util.{Failure, Success}

class BlastModelTest extends BlastSpec {

  def autoClose[I <: AutoCloseable, R](ac:I)(f:I=>R) : R = {
    try {
      f(ac)
    } finally {
      ac.close()
    }
  }

  "A model" should "round-trip via the dsl" in {
    val model: Model = new Model
    val myStruct =
      model
        .addStruct("com.myco.MyStruct")
        .addAttribute(BAttr("pString", BString))
        .addAttribute(BAttr("pNumber", BNumber))
        .addAttribute(BAttr("pBool", BBoolean))

    model
      .addStruct("com.myco.MyStruct2")
      .addAttribute(BAttr("pString2", BString))

    val myEnum =
      model
        .addEnum("com.myco.MyEnum", "Bad", "Average", "Good")

    model
        .addStruct("com.myco.MyStruct3")
        .addAttribute(BAttr("pStruct", myStruct, isArray = true))
        .addAttribute(BAttr("pEnum", myEnum, isArray = true))

    val str = model.toDsl

    withParsedModel(str) { m2 =>
      assert(model === m2)
    }
  }

  it should "parse from a test file" in {
    autoClose(new InputStreamReader(getClass.getResourceAsStream("/test.blast"))) { r =>
      val model = ModelParser.parseModel(r) getOrElse fail
      val myStruct = model.getType("com.myapp.MyStruct").get.asInstanceOf[BStruct]
      assert(myStruct.getAttribute("pString").isDefined)
    }
  }


  it should "fail to parse from an invalid string" in {
    val ex = intercept[Exception] {
      ModelParser.parseModel("Hey hoooo").get
    }
    assert("`command' expected but `H' found" === ex.getMessage)
  }

  "A parsed enum" should " have the values" in {
    withParsedModel(
      """enum MyEnum {
        |  Foo
        |  Bar
        |  Baz
        |}
      """) { model =>
        val e = model.getType("MyEnum")
        assert(e.nonEmpty)
        val bEnum = e.get.asInstanceOf[BEnum]
        assert(bEnum.values === List("Foo", "Bar", "Baz"))
    }
  }

  "A parsed struct with array" should "have the array defined" in {
    withParsedModel(
      """struct com.myco.MyStrings {
        |  pStrings:string*
        |}
      """) { model =>
      val myStrings2 = model.getType("com.myco.MyStrings").get.asInstanceOf[BStruct]
      val pStrings = myStrings2.getAttribute("pStrings").get
      assert(pStrings.isArray)
    }
  }

  "A command" should "parse" in {
    withParsedModel(
      """struct S1 {
        |  foo:string
        |}
        |struct S2 {
        |  bar:string
        |}
        |command C : S1 -> S2
      """) { model =>
      val cmd = model.getCommands.filter(_.name == "C").head
      val s1 = model.getStruct("S1").get
      val s2 = model.getType("S2").get
      assert(cmd.input == s1)
      assert(cmd.output == s2)
    }
  }

  private def withParsedModel(str:String)(f:Model => Unit) = {
    ModelParser.parseModel(str.stripMargin) match {
      case Success(model2) =>
        f
      case Failure(err) =>
        fail(err)
    }
  }


}
