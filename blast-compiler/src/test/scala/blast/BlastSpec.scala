package blast

import org.scalatest._

abstract class BlastSpec extends FlatSpec
  with Matchers
  with OptionValues
  with Inside
  with Inspectors