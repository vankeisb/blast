package blast.b4j;

import javax.json.stream.JsonGenerator;

public interface Serializer<T extends Struct> {
	void serialize(T struct, JsonGenerator g) throws SerializationException;
}
