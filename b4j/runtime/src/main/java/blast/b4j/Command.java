package blast.b4j;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public interface Command<I extends Struct,O extends Struct> {
	void execute(I input, Callback<O> callback);
	Class<I> getInputType();
	Class<O> getOutputType();
	I deserializeInput(Reader reader) throws IOException;
	void serializeOutput(O output, Writer out) throws IOException;
}
