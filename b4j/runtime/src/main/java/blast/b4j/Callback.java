package blast.b4j;

import java.io.IOException;

public interface Callback<O> {
	void apply(O result, Throwable err) throws IOException;
}
