package blast.b4j;

import javax.json.stream.JsonParser;

public interface Deserializer<T extends Struct> {
	T deserialize(JsonParser parser) throws SerializationException;
}
