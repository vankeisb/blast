libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.6"
libraryDependencies += "javax.json" % "javax.json-api" % "1.1"
libraryDependencies += "org.glassfish" % "javax.json" % "1.1"

// disable using the Scala version in output paths and artifacts
crossPaths := false