lazy val commonSettings = Seq(
    organization := "com.pojosontheweb",
    version := "LATEST-SNAPSHOT",
    scalaVersion := "2.12.3",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"
)

lazy val root = (project in file("."))
  .settings(
    commonSettings,
    name := "blast"
  )
  .aggregate(compiler,b4jRuntime)

lazy val compiler = (project in file("blast-compiler"))
  .settings(
    commonSettings,
    name := "blast-compiler"
  )

lazy val b4jRuntime = (project in file("b4j/runtime"))
  .settings(
    commonSettings,
    name := "blast-java-runtime"
  )